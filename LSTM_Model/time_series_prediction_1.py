# In time series prediction for stocks, we do not predict the exact value of a stock in the
# future, rather we predict if the stock is going to rise or fall at a given time instance

# Import the necessary libraries
from pandas_datareader import data ## access uptodate data from the web
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import urllib, json
import os
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler 
import difflib
'''
Stock prices come in several different flavours. They are,

* Open: Opening stock price of the day
* Close: Closing stock price of the day
* High: Highest stock price of the data
* Low: Lowest stock price of the day
'''
## get the data from vantage and load it to pandas dataframe
data_source = 'kaggle'

if data_source == 'alphavantage':
    ## load data from alphavantage
    api_key = 'EKRXVUXVD7WVVD1S'
    ## American airlines stock market price
    ticker = 'AAL'
    ## Json file with AAL stock market price for last 20 years
    url_string = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=%s&outputsize=full&apikey=%s"%(ticker,api_key)
    ## save data to this file
    file_to_save = 'stock_market_data-%s.csv'%ticker
    
    ## if you havent saved the data, go to the url and get the data
    ## and store date, low, high, volume, close, open values to a Pandas dataframe
    if not os.path.exists(file_to_save):
        with urllib.request.urlopen(url_string) as url:
            data = json.loads(url.read().decode())
            ## extract stock market data
            data = data['Time Series (Daily)']
            df = pd.DataFrame(columns=['Date', 'Low', 'High', 'Close', 'Open'])
            for k, v in data.items():
                date = dt.datetime.strptime(k, '%Y-%m-%d')
                data_row = [date.date(), float(v['3. low']), float(v['2. high']),
                            float(v['4. close']), float(v['1. open'])
                ]
                df.loc[-1,:] = data_row
                df.index = df.index + 1
        print('Data saved to : %s'%file_to_save)  
        df.to_csv(file_to_save)
    ## if file alread exists
    else:
        print('File already exists. Loading data from CSV')
        df = pd.read_csv(file_to_save)

else:
    
    # ====================== Loading Data from Kaggle ==================================
    # You will be using HP's data. Feel free to experiment with other data. 
    # But while doing so, be careful to have a large enough dataset and also pay attention to the data normalization
    df = pd.read_csv(os.path.join('Stocks','hpq.us.txt'),delimiter=',',usecols=['Date','Open','High','Low','Close'])
    print('Loaded data from the Kaggle repository')

## check the data before proceeding
## sorte dataframe by date
df = df.sort_values('Date')
## double check the results
print(df.head())
print(df.shape[0])
## lets visualize the data
plt.figure(figsize=(18, 9))
plt.plot(range(df.shape[0]), (df['Low'] + df['High']) / 2.0)
plt.xticks(range(0,df.shape[0],500),df['Date'].loc[::500],rotation=45)
plt.xlabel('Date',fontsize=18)
plt.ylabel('Mid Price',fontsize=18)
#plt.show()

## Data preparation
## for prediction, we will use the mid price (high+low) / 2
hight_price = df.loc[:, 'High'].values
low_price = df.loc[:, 'Low'].values

mid_price = (hight_price + low_price) / 2.0

## Now we can split train test sets
train_data = mid_price[:11000] # 70% for training
test_data = mid_price[11000:] # 30% for testing
## Now lets normalize the data using MinMaxScaler, where data would be between 0 and 1
## we can structure the data to have a shape of [data_size, num_features]
scaler = MinMaxScaler()
train_data = train_data.reshape(-1, 1)
test_data = test_data.reshape(-1, 1)

## lets normalize the data using a window. The size of the window = train_data_size / 4
smoothing_window_size = 2500#int(train_data.shape[0] / 4)
print('window size: {}'.format(smoothing_window_size))

for di in range(0, 10000, smoothing_window_size):
    scaler.fit(train_data[di:di+smoothing_window_size, :])
    #print(scaler.fit(train_data[di:di+smoothing_window_size, :]))
    train_data[di:di+smoothing_window_size, :] = scaler.transform(train_data[di:di+smoothing_window_size, :])
    #print(train_data[di:di+smoothing_window_size, :])
## normalie the rest of train data
scaler.fit(train_data[di+smoothing_window_size:, :])
train_data[di+smoothing_window_size:, :] = scaler.transform(train_data[di+smoothing_window_size:, :])

## reshape the train data back to the shape [data_size]
train_data = train_data.reshape(-1)
## Normalize the test data according to the train data
scaler_2 = MinMaxScaler()
scaler_2.fit(test_data)
test_data = scaler_2.transform(test_data).reshape(-1)
print(test_data)
## Now use the moving average method to smoothen the data
## using exponential moving average,
EMA = 0
gamma = 1.0

for ti in range(train_data.shape[0]):
    EMA = gamma * train_data[ti] + (1 - gamma) * EMA
    train_data[ti] = EMA

## for visualization
all_mid_data = np.concatenate([train_data, test_data], axis=0)
print(np.min(all_mid_data))
print(np.max(all_mid_data))

print(len(all_mid_data))

####################################### One step ahead prediction using averaging ###################################
## one predict the stock price one step ahead by averaging the observed stock price. For averaging, standard average
## and exponential moving average can be used. If you try to predict few step aheads using averaging, it will fail.
## lets perform the prediction by averaging and see quality by plotting the predictions and calculating the MSE

## the normal averaging method
## x(t+1) = (1 / N) * sum(x(i) ; i = t-N:t) : observed window from t to t-N

window_size = 100
N = train_data.size
std_avg_prediction = []
std_avg_x = []
mse_errors = []

for pred_idx in range(window_size, N):
    if pred_idx >= N:
        date = dt.datetime.strptime(k, '%Y-%m-%d').date() + dt.timedelta(day=1)
    else:
        date = df.loc[pred_idx, 'Date']

    ## do standard averaging
    std_avg_prediction.append(np.mean(train_data[pred_idx - window_size:pred_idx]))
    ## append the error
    mse_errors.append((std_avg_prediction[-1] - train_data[pred_idx]) ** 2)
    std_avg_x.append(date)

print('MSE error for standard averaging: %.5f'%(0.5*np.mean(mse_errors)))

plt.figure(figsize = (18,9))
plt.plot(range(df.shape[0]),all_mid_data,color='b',label='True')
plt.plot(range(window_size,N),std_avg_prediction,color='orange',label='Prediction')
#plt.xticks(range(0,df.shape[0],50),df['Date'].loc[::50],rotation=45)
plt.xlabel('Date')
plt.ylabel('Mid Price')
plt.legend(fontsize=18)
#plt.show()

## The data batch generator
class DataGeneratorSeq(object):
    def __init__(self, price, batch_size, num_unroll):
        self._price = price
        self._price_length = len(self._price) - num_unroll
        print(self._price_length)
        self._batch_size = batch_size
        self._num_unroll = num_unroll
        self._segments = self._price_length // self._batch_size
        print(self._segments)
        self._cursor = [offset * self._segments for offset in range(self._batch_size)]
        print(self._cursor)

    def next_batch(self):
        batch_data = np.zeros((self._batch_size), dtype=np.float32)
        batch_labels = np.zeros((self._batch_size), dtype=np.float32)

        for b in range(self._batch_size):
            if self._cursor[b] + 1 >= self._price_length:
                self._cursor[b] = np.random.randint(0, (b+1) * self._segments)
            
            batch_data[b] = self._price[self._cursor[b]]
            batch_labels[b] = self._price[self._cursor[b]+np.random.randint(0, 5)]
            ## for every new batch, increase the cursor index
            self._cursor[b] = (self._cursor[b] + 1) % self._price_length
        #print(self._cursor)

        return batch_data, batch_labels

    def unroll_batches(self):
        unroll_data, unroll_labels = [], []
        init_data, init_label = None, None

        for ui in range(self._num_unroll):
            data, labels = self.next_batch()
            unroll_data.append(data)
            unroll_labels.append(labels)

        return unroll_data, unroll_labels

    def reset_indices(self):
        for b in range(self._batch_size):
            self._cursor[b] = np.random.randint(0,min((b+1)*self._segments,self._prices_length-1))

dg = DataGeneratorSeq(train_data,10,5)
u_data, u_labels = dg.unroll_batches()

for ui,(dat,lbl) in enumerate(zip(u_data,u_labels)):   
    print('\n\nUnrolled index %d'%ui)
    dat_ind = dat
    lbl_ind = lbl
    print('\tInputs: ',dat )
    print('\n\tOutput:',lbl)

## The num_unroll discussed above is a hyper parameter for BPTT.
## the batch_size is how many data samples you consider in single time step

## Lets define the hyper parameters for the lstm
D = 1 # dimensionality of data, since the data is 1-D, this would be 1
num_unrollings = 50 # number of time-steps you look into the future
batch_size = 500 # number of samples in a batch
num_nodes = [400, 400, 300] # Number of hidden nodes in each layer of the deep lstm stack
n_layers = len(num_nodes) # number of hidden layers
dropout = 0.2 # dropout amount

tf.reset_default_graph() # Importent if you run this multiple times

## now lets define placeholders for input output data
## here we have 50 placeholders which is equal to num_unrollings.
train_inputs, train_outputs = [], []

## you unroll the input over time, defining place-holder for each time-step
for ui in range(num_unrollings):
    train_inputs.append(tf.placeholder(shape=[batch_size, D], dtype=tf.float32, name='train_inputs_%d' %ui))
    train_outputs.append(tf.placeholder(shape=[batch_size, 1], dtype=tf.float32, name='train_outputs_%d' %ui))

## In this model, we have 3 lstm layers and 1 linear regression layer. the linear regression layer takes the
## output from the last lstm layer and output the prediction of next time step
## a dropout layer here, is implemented to prevent the overfitting

## define the lstm layers
lstm_cells = [tf.contrib.rnn.LSTMCell(
                num_units=num_nodes[li],
                state_is_tuple=True,
                initializer=tf.contrib.layers.xavier_initializer()
                )
for li in range(n_layers)]

## The dropout wrapper for the lstm layers, This adds dropouts to inputs, outputs and states lstm cell
## the dropouts are introduced to the layers individually
drop_lstm_cells = [tf.contrib.rnn.DropoutWrapper(lstm, input_keep_prob=1.0,
                                        output_keep_prob=1.0-dropout,
                                        state_keep_prob=1.0-dropout
) for lstm in lstm_cells]

## Now lets use MultiRNNCell of tensorflow to encapsulate the drop out cells and
## lstm cells
## this creates an lstm super cell consisting of sequence of lstm cells
## this is necessary when you give the cell to dynamic_rnn to compute the output state pair
## in the case of manually running the lstm it is not necessary
drop_multi_cells = tf.contrib.rnn.MultiRNNCell(drop_lstm_cells)
multi_cells = tf.contrib.rnn.MultiRNNCell(lstm_cells)

## Initiate the linear regression layer
w = tf.get_variable('w', shape=[num_nodes[-1], 1], initializer=tf.contrib.layers.xavier_initializer())
b = tf.get_variable('b', initializer=tf.random_uniform([1], -0.1, 0.1))

## create cell state and hidden state variables to maintain the state of the lstm
c, h = [], []
initial_state = []

for li in range(n_layers):
    c.append(tf.Variable(tf.zeros([batch_size, num_nodes[li]]), trainable=False))
    h.append(tf.Variable(tf.zeros([batch_size, num_nodes[li]]), trainable=False))
    initial_state.append(tf.contrib.rnn.LSTMStateTuple(c[li], h[li]))

## do several tensor transformations, because dynamic_rnn needs the data to be in a
## specific format ([num_unrolling, batch_size, D])
all_inputs = tf.concat([tf.expand_dims(t, 0) for t in train_inputs], axis=0)

## get the all_outputs with shape=[num_unrolling, batch_size, 1]
all_lstm_outputs = tf.nn.dynamic_rnn(drop_multi_cells, all_inputs, initial_state=tuple(initial_state),
                                time_major=True, dtype=tf.float32)

## In dynamic_rnn it outputs a 3D tensor, first element of first dim is ouputs
## the second elemet of the first dim is the cell state and hidden state
## therefore get the states of all the hidden layers to a variable                              
state = all_lstm_outputs[1]
print(all_lstm_outputs[0])
all_lstm_outputs = tf.reshape(all_lstm_outputs[0], [batch_size*num_unrollings, num_nodes[-1]])

all_outputs = tf.nn.xw_plus_b(all_lstm_outputs, w, b)

split_outputs = tf.split(all_outputs, num_unrollings, axis=0)

## when calculating the loss be aware of the form, because your calculating the loss of all
## unrolled steps at the same time
print('Define training loss')
loss = 0.0

## state[li][0] is the cell state in layers
## state[li][1] is the hidden state in layers
## execute the control_input (one within the brackets of contex manager) before computing the loss
## this is done in order to check the consistency of the network
with tf.control_dependencies([tf.assign(c[li], state[li][0]) for li in range(n_layers)]+
                             [tf.assign(h[li], state[li][1]) for li in range(n_layers)]):
  for ui in range(num_unrollings):
    loss += tf.reduce_mean(0.5*(split_outputs[ui]-train_outputs[ui])**2)

print('learning rate decay operation')
global_step = tf.Variable(0, trainable=False)
inc_gstep = tf.assign(global_step, global_step + 1)
tf_learning_rate = tf.placeholder(shape=None, dtype=tf.float32)
tf_min_learning_rate = tf.placeholder(shape=None, dtype=tf.float32)
learning_rate = tf.maximum(tf.train.exponential_decay(tf_learning_rate, global_step,
                            decay_rate=0.5, decay_steps=1, staircase=True), tf_min_learning_rate)

## Optimizer
print('TF Optimizer operation')
optimizer = tf.train.AdamOptimizer(learning_rate)
## The compute_gradients returns the pair of computed gradients and the corresponding variables
## (the corresponding variables are the weights). Note that gradient = None is possible for a variable 
## (a gradient could none-exist)
## here the * indicates unzipping of a zipped pair (here unzipping optimizer.compute_gradients will return
## a tuple of gradients and there corresponding variables)
gradients, v = zip(*optimizer.compute_gradients(loss))
## clip values of multiple tensors by the ratio of the sum of their norms
## clipping equation is t_list[i] * clip_norm / max(global_norm, clip_norm)
## the global norm = sqrt(sum[l2norm(t) for t in t_list])
## in the following case, clip_norm = 0.5 and t_list = gradients
gradients, _ = tf.clip_by_global_norm(gradients, 0.5)
## Apply the computed gradients and the corresponding variables
optimizer = optimizer.apply_gradients(zip(gradients, v))
print('\tAll done')

print('Defining prediction related TF functions')

sample_inputs = tf.placeholder(dtype=tf.float32, shape=[1, D])

## Maintaining LSTM state for prediction stage
sample_c, sample_h, initial_sample_state = [], [], []
## here the batch size of inputs is 1, where each batch is size of num_unrollings
for li in range(n_layers):
    sample_c.append(tf.Variable(tf.zeros([1, num_nodes[li]]), trainable=False))
    sample_h.append(tf.Variable(tf.zeros([1, num_nodes[li]]), trainable=False))
    initial_sample_state.append(tf.contrib.rnn.LSTMStateTuple(sample_c[li], sample_h[li]))

## create a op that groups multiple operations
reset_sample_state = tf.group(*[tf.assign(sample_c[li], tf.zeros([1, num_nodes[li]])) for li in range(n_layers)],
                                *[tf.assign(sample_h[li], tf.zeros([1, num_nodes[li]])) for li in range(n_layers)])

## compute the lstm output, since this is the prediction stage, we use multi_cells instead of drop_multi_cells
sample_outputs, sample_state = tf.nn.dynamic_rnn(multi_cells, tf.expand_dims(sample_inputs, 0), 
                                initial_state=tuple(initial_sample_state), time_major=True,
                                dtype=tf.float32)

with tf.control_dependencies([tf.assign(sample_c[li], sample_state[li][0]) for li in range(n_layers)] +
                            [tf.assign(sample_h[li], sample_state[li][1]) for li in range(n_layers)]):
    ## compute the predictions using the lstm outputs
    sample_prediction = tf.nn.xw_plus_b(tf.reshape(sample_outputs, [1, -1]), w, b)

print('\tAll done')

## Now lets train the neural network
epochs = 1000
valid_summery = 1 # intervals you make the test predictions
n_predict_once = 50 # Number of steps you continuously predict for
train_seq_length = train_data.size # full length of training data

train_mse_ot = [] # Accumulate train losses
test_mse_ot = [] # Accumulate test losses
predictions_over_time = [] # Accumulate predictions

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    ## use for decaying learning rate
    loss_nondecrease_count = 0
    loss_nondecrease_treshold = 2 # if test error hasnt increased in this many steps, decrease the learning rate

    print('Initialized')
    average_loss = 0

    ## define data generator object
    data_gen = DataGeneratorSeq(train_data, batch_size, num_unrollings)
    x_axis_seq = []

    ## point we start our predictions from
    test_point_seq = np.arange(11000, 12000, 50).tolist()
    print(test_point_seq)
    print(len(test_point_seq))

    for ep in range(epochs):
        ## Training step #############################
        for step in range(train_seq_length // batch_size):
            u_data, u_labels = data_gen.unroll_batches()
            feed_dict = {}
            for ui, (dat, lbl) in enumerate(zip(u_data, u_labels)):
                ## the ui here indices the unrollings. so this for loop will run for
                ## 50 iterations because num_unrollings = 50
                ## feed the train_inputs and train_outputs placeholders with current
                ## unrolled element
                feed_dict[train_inputs[ui]] = dat.reshape(-1, 1)
                feed_dict[train_outputs[ui]] = lbl.reshape(-1, 1)

            feed_dict.update({learning_rate: 0.0001, tf_min_learning_rate: 0.000001})

            _, l = sess.run([optimizer, loss], feed_dict=feed_dict)

            average_loss += l

        ## validation step ############################
        if (ep+1) % valid_summery == 0:
            average_loss = average_loss / (valid_summery * train_seq_length // batch_size)
            ## average loss
            if (ep+1) % valid_summery == 0:
                print('Average loss at epoch %d: %f' % (ep+1, average_loss))

            train_mse_ot.append(average_loss)
            average_loss = 0 ## reset loss
            
            predictions_seq = []
            mse_test_loss_seq = []
        
        ## update state and make predictions ###########
        for w_i in test_point_seq:
            mse_test_loss = 0.0
            out_predictions = []

            if (ep+1) - valid_summery == 0:
                ## only calculate the x-axis values in the first validation epoch
                x_axis = []

            ## Feed-in the recent past behavior of stock prices to 
            ## make predictions from that point onwards
            '''
            for tr_i in range(w_i - num_unrollings + 1, w_i - 1):
                current_price = all_mid_data[tr_i]
                feed_dict[sample_inputs] = np.array(current_price).reshape(1, 1)
                _ = sess.run(sample_prediction, feed_dict=feed_dict)
            '''
            feed_dict = {}

            current_price = all_mid_data[w_i - 1]

            feed_dict[sample_inputs] = np.array(current_price).reshape(1, 1)
            #print(feed_dict)        
            ## Make predictions for this many steps
            ## each prediction uses previous predictions as their current input
            for pred_i in range(n_predict_once):
                pred = sess.run(sample_prediction, feed_dict=feed_dict)
                out_predictions.append(np.asscalar(pred))
                feed_dict[sample_inputs] = np.asarray(pred).reshape(-1, 1)

                if (ep+1) - valid_summery == 0:
                    ## only calculate x-axis values in the first validation epoch
                    x_axis.append(w_i+pred_i)

                mse_test_loss += 0.5 * (pred-all_mid_data[w_i+pred_i])**2
                #print(pred)
                #print(all_mid_data[w_i + pred_i])
            sess.run(reset_sample_state)

            predictions_seq.append(np.array(out_predictions))

            mse_test_loss /= n_predict_once
            mse_test_loss_seq.append(mse_test_loss)

            if (ep+1) - valid_summery == 0:
                x_axis_seq.append(x_axis)

        current_test_mse = np.mean(mse_test_loss_seq)

        ## learning rate decay logic
        if len(test_mse_ot) > 0 and current_test_mse > min(test_mse_ot):
            loss_nondecrease_count += 1
        else:
            loss_nondecrease_count = 0

        if loss_nondecrease_count > loss_nondecrease_treshold:
            sess.run(inc_gstep)
            loss_nondecrease_count = 0
            print('\tDecreasing learning rate by 0.5')

        test_mse_ot.append(current_test_mse)
        print('\tTest MSE: %.5f'%np.mean(mse_test_loss_seq))
        predictions_over_time.append(predictions_seq)
        print('\tFinished Predictions')
        
best_prediction_epoch = epochs - 2

plt.figure(figsize=(18,18))
plt.subplot(2,1,1)
plt.plot(range(df.shape[0]), all_mid_data, color='b')

## plotting how the prediction change over time
start_alpha = 0.25
alpha = np.arange(start_alpha, 1.1, (1.0-start_alpha)/len(predictions_over_time[::3]))

for p_i, p in enumerate(predictions_over_time[::3]):
    for x_val, y_val in zip(x_axis_seq, p):
        plt.plot(x_val, y_val, color='r', alpha=alpha[p_i])

plt.title('Evolution of Test Predictions Over Time',fontsize=8)
plt.xlabel('Date',fontsize=8)
plt.ylabel('Mid Price',fontsize=8)
plt.xlim(11000, 12500)

plt.subplot(2,1,2)
## predicting the best test prediction you got
plt.plot(range(df.shape[0]), all_mid_data, color='b')
for x_val, y_val in zip(x_axis_seq, predictions_over_time[best_prediction_epoch]):
    plt.plot(x_val, y_val, color='r')

plt.title('Best Test Predictions Over Time',fontsize=8)
plt.xlabel('Date',fontsize=8)
plt.ylabel('Mid Price',fontsize=8)
plt.xlim(11000, 12500)
plt.show()















